#include "memman.h"

#include <map>
#include <stack>
#include <tuple>
#include <string>
#include <cuda_runtime.h>
#include <curand_kernel.h>
#include <cuda.h>
#include "cuda_util.h"

#include <mutex>

using namespace std;

enum {
  ZERO_GPU = 0b0001,
  ZERO_CPU = 0b0010
};

// tuple <cpu_ptr, gpu_ptr, size>
static map<string, tuple<void*, void*, size_t>> alloced;
static thread_local map<string, tuple<void*, void*, size_t>> allocedTL;

// a stack per stream
static map<void*, stack<tuple<void*, void*, size_t>>> adHocMem;

static thread_local string selectedMem;

static mutex memman_mutex;

static int zero(cudaStream_t stream, int option);

int memman_alloc_dual(const char *key, size_t size, int options)
{
  auto k = string(key);
  void *dev, *host;

  if (options & MEMMAN_THRLC) {
    auto find = allocedTL.find(k);
    if (find != allocedTL.end()) return -1; // exists
    CUDA_CHECK_ERROR(cudaMallocHost(&host, size), "");
    CUDA_ALLOC(dev, size);
    auto t = make_tuple(host, dev, size);
    allocedTL.insert(find, make_pair(k, t));
  } else { // global
    auto find = alloced.find(k);
    if (find != alloced.end()) return -1; // exists
    CUDA_CHECK_ERROR(cudaMallocHost(&host, size), "");
    CUDA_ALLOC(dev, size);
    auto t = make_tuple(host, dev, size);
    alloced.insert(find, make_pair(k, t));
  }

  selectedMem = k;
  return 0;
}

int memman_alloc_gpu(const char *key, size_t size, void *host, int options)
{
  auto k = string(key);
  void *dev;

  if (options & MEMMAN_THRLC) {
    auto find = allocedTL.find(k);
    if (find != allocedTL.end()) return -1; // exists
    CUDA_ALLOC(dev, size);
    auto t = make_tuple(host, dev, size);
    allocedTL.insert(find, make_pair(k, t));
  } else { // global
    auto find = alloced.find(k);
    if (find != alloced.end()) return -1; // exists
    CUDA_ALLOC(dev, size);
    auto t = make_tuple(host, dev, size);
    alloced.insert(find, make_pair(k, t));
  }

  selectedMem = k;
  return 0;
}

int memman_alloc_cpu(const char *key, size_t size, void *dev, int options)
{
  auto k = string(key);
  void *host;

  if (options & MEMMAN_THRLC) {
    auto find = allocedTL.find(k);
    if (find != allocedTL.end()) return -1; // exists
    CUDA_CHECK_ERROR(cudaMallocHost(&host, size), "");
    auto t = make_tuple(host, dev, size);
    allocedTL.insert(find, make_pair(k, t));
  } else { // global
    auto find = alloced.find(k);
    if (find != alloced.end()) return -1; // exists
    CUDA_CHECK_ERROR(cudaMallocHost(&host, size), "");
    auto t = make_tuple(host, dev, size);
    alloced.insert(find, make_pair(k, t));
  }

  selectedMem = k;
  return 0;
}

int memman_select(const char *key)
{
  auto k     = string(key);
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find == alloced.end() && fndLT == allocedTL.end()) {
    // not found
    return -1;
  }
  selectedMem = k; // TODO: also externalize the iterator
  return 0;
}

int memman_free_dual()
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find != alloced.end()) {
    auto t       = find->second;
    auto hostMem = get<0>(t);
    auto devMem  = get<1>(t);
    cudaFreeHost(hostMem);
    cudaFree(devMem);
    alloced.erase(find);
  } else if (fndLT != allocedTL.end()) {
    auto t       = fndLT->second;
    auto hostMem = get<0>(t);
    auto devMem  = get<1>(t);
    cudaFreeHost(hostMem);
    cudaFree(devMem);
    allocedTL.erase(fndLT);
  } else {
    // not found
    return -1;
  }

  return 0;
}

int memman_free_cpu()
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find != alloced.end()) {
    auto t       = find->second;
    auto hostMem = get<0>(t);
    cudaFreeHost(hostMem);
    alloced.erase(find);
  } else if (fndLT != allocedTL.end()) {
    auto t       = fndLT->second;
    auto hostMem = get<0>(t);
    cudaFreeHost(hostMem);
    allocedTL.erase(fndLT);
  } else {
    // not found
    return -1;
  }

  return 0;
}

int memman_free_gpu()
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find != alloced.end()) {
    auto t       = find->second;
    auto devMem  = get<1>(t);
    cudaFree(devMem);
    alloced.erase(find);
  } else if (fndLT != allocedTL.end()) {
    auto t       = fndLT->second;
    auto devMem  = get<1>(t);
    cudaFree(devMem);
    allocedTL.erase(fndLT);
  } else {
    // not found
    return -1;
  }

  return 0;
}

int memman_set_cpu(void *host, size_t size)
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find != alloced.end()) {
    auto oldT = find->second;
    auto dev  = get<1>(oldT);
    size_t newS = size > 0 ? size : get<2>(oldT);
    auto newT = make_tuple(host, dev, newS);
    find = alloced.erase(find);
    alloced.insert(find, make_pair(k, newT));
  } else if (fndLT != allocedTL.end()) {
    auto oldT = fndLT->second;
    auto dev  = get<1>(oldT);
    size_t newS = size > 0 ? size : get<2>(oldT);
    auto newT = make_tuple(host, dev, newS);
    fndLT = allocedTL.erase(fndLT);
    allocedTL.insert(fndLT, make_pair(k, newT));
  } else {
    // not found
    return -1;
  }

  return 0;
}

int memman_set_gpu(void *dev, size_t size)
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find != alloced.end()) {
    auto oldT = find->second;
    auto host = get<0>(oldT);
    size_t newS = size > 0 ? size : get<2>(oldT);
    auto newT = make_tuple(host, dev, newS);
    find = alloced.erase(find);
    alloced.insert(find, make_pair(k, newT));
  } else if (fndLT != allocedTL.end()) {
    auto oldT = fndLT->second;
    auto host = get<0>(oldT);
    size_t newS = size > 0 ? size : get<2>(oldT);
    auto newT = make_tuple(host, dev, newS);
    fndLT = alloced.erase(fndLT);
    allocedTL.insert(fndLT, make_pair(k, newT));
  } else {
    // not found
    return -1;
  }

  return 0;
}

void *memman_get_gpu(size_t *size)
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find != alloced.end()) {
    auto t = find->second;
    if (size != NULL) *size = get<2>(t);
    return get<1>(t);
  } else if (fndLT != allocedTL.end()) {
    auto t = fndLT->second;
    if (size != NULL) *size = get<2>(t);
    return get<1>(t);
  }

  // not found
  return NULL;
}

void *memman_get_cpu(size_t *size)
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);

  if (find != alloced.end()) {
    auto t = find->second;
    if (size != NULL) *size = get<2>(t);
    return get<0>(t);
  } else if (fndLT != allocedTL.end()) {
    auto t = fndLT->second;
    if (size != NULL) *size = get<2>(t);
    return get<0>(t);
  }

  // not found
  return NULL;
}

int memman_zero_dual(void *stream)
{
  cudaStream_t strm = (cudaStream_t)stream;
  return zero(strm, ZERO_CPU|ZERO_GPU);
}

int memman_zero_cpu(void *stream)
{
  cudaStream_t strm = (cudaStream_t)stream;
  return zero(strm, ZERO_CPU);
}

int memman_zero_gpu(void *stream)
{
  cudaStream_t strm = (cudaStream_t)stream;
  return zero(strm, ZERO_GPU);
}

void* memman_ad_hoc_alloc(void* stream, void *host, size_t size)
{
  void *dev;
  CUDA_ALLOC(dev, size);
  auto newT = make_tuple(host, dev, size);
  memman_mutex.lock();
  adHocMem[stream].push(newT); // creates if not found
  memman_mutex.unlock();
  return dev;
}

void* memman_ad_hoc_free(void* stream)
{
  memman_mutex.lock();
  if (adHocMem[stream].empty()) {
    memman_mutex.unlock();
    return NULL;
  }
  auto oldT = adHocMem[stream].top();
  void *host = get<0>(oldT);
  void *dev = get<1>(oldT);
  adHocMem[stream].pop();
  memman_mutex.unlock();
  CUDA_CHECK_ERROR(cudaFree(dev), "free");
  return host;
}

void* memman_ad_hoc_zero(void *stream)
{
  memman_mutex.lock();
  if (adHocMem[stream].empty()) {
    memman_mutex.unlock();
    return NULL;
  }
  cudaStream_t strm = (cudaStream_t)stream;
  auto oldT = adHocMem[stream].top();
  void *dev = get<1>(oldT);
  size_t size = get<2>(oldT);
  memman_mutex.unlock();
  if (strm != NULL) {
    CUDA_CHECK_ERROR(cudaMemsetAsync(dev, 0, size, strm), "");
  } else {
    CUDA_CHECK_ERROR(cudaMemset(dev, 0, size), "");
  }
  return dev;
}

void* memman_ad_hoc_cpy(void *stream)
{
  if (adHocMem[stream].empty()) return NULL;
  memman_mutex.lock();
  cudaStream_t strm = (cudaStream_t)stream;
  auto oldT = adHocMem[stream].top();
  void *host = get<0>(oldT);
  void *dev = get<1>(oldT);
  size_t size = get<2>(oldT);
  memman_mutex.unlock();

  if (strm != NULL) {
    CUDA_CPY_TO_DEV_ASYNC(dev, host, size, strm);
  } else {
    CUDA_CPY_TO_DEV(dev, host, size);
  }
  return dev;
}

void* memman_ad_hoc_retrieve(void *stream)
{
  if (adHocMem[stream].empty()) return NULL;
  memman_mutex.lock();
  cudaStream_t strm = (cudaStream_t)stream;
  auto oldT = adHocMem[stream].top();
  void *host = get<0>(oldT);
  void *dev = get<1>(oldT);
  size_t size = get<2>(oldT);
  memman_mutex.unlock();

  if (strm != NULL) {
    CUDA_CPY_TO_HOST_ASYNC(host, dev, size, strm);
  } else {
    CUDA_CPY_TO_HOST(host, dev, size);
  }
  return host;
}

int memman_cpy_to_cpu(void *strm)
{
  cudaStream_t stream = (cudaStream_t)strm;
  auto k    = selectedMem;
  auto find = alloced.find(k);
  auto fndLT = allocedTL.find(k);
  tuple<void*, void*, size_t> t;

  if (find != alloced.end()) {
    t = find->second;
  } else if (fndLT != allocedTL.end()) {
    t = fndLT->second;
  } else {
    // not found
    return -1;
  }

  auto hostMem = get<0>(t);
  auto devMem  = get<1>(t);
  auto memSize = get<2>(t);

  if (hostMem == NULL || devMem == NULL) {
    return -1;
  }

  if (stream != NULL) {
    CUDA_CPY_TO_HOST_ASYNC(hostMem, devMem, memSize, stream);
  } else {
    CUDA_CPY_TO_HOST(hostMem, devMem, memSize);
  }

  return 0;
}

int memman_cpy_to_gpu(void *strm)
{
  cudaStream_t stream = (cudaStream_t)strm;
  auto k    = selectedMem;
  auto find = alloced.find(k);
  auto fndLT = allocedTL.find(k);
  tuple<void*, void*, size_t> t;

  if (find != alloced.end()) {
    t = find->second;
  } else if (fndLT != allocedTL.end()) {
    t = fndLT->second;
  } else {
    // not found
    return -1;
  }

  auto hostMem = get<0>(t);
  auto devMem  = get<1>(t);
  auto memSize = get<2>(t);

  if (hostMem == NULL || devMem == NULL) {
    return -1;
  }

  if (stream != NULL) {
    CUDA_CPY_TO_DEV_ASYNC(devMem, hostMem, memSize, stream);
  } else {
    CUDA_CPY_TO_DEV(devMem, hostMem, memSize);
  }
  return 0;
}

static int zero(cudaStream_t stream, int option)
{
  auto k     = selectedMem;
  auto find  = alloced.find(k);
  auto fndLT = allocedTL.find(k);
  tuple<void*, void*, size_t> t;

  if (find != alloced.end()) {
    t = find->second;
  } else if (fndLT != allocedTL.end()) {
    t = fndLT->second;
  } else {
    // not found
    return -1;
  }

  auto hostMem = get<0>(t);
  auto devMem  = get<1>(t);
  auto memSize = get<2>(t);

  if (option & ZERO_GPU) {
    if (devMem == NULL) {
      return -1;
    }
    if (stream != NULL) {
      CUDA_CHECK_ERROR(cudaMemsetAsync(devMem, 0, memSize, stream), "");
    } else {
      CUDA_CHECK_ERROR(cudaMemset(devMem, 0, memSize), "");
    }
  }
  if (option & ZERO_CPU) {
    if (hostMem == NULL) {
      return -1;
    }
    memset(hostMem, 0, memSize);
  }
  return 0;
}
