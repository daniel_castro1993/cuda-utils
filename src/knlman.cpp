#include "knlman.h"

#include <map>
#include <tuple>
#include <list>
#include <vector>

#include "cuda_util.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "helper_cuda.h"
#include "helper_timer.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>
#include <cuda.h>

using namespace std;

#define DEFAULT_BLOCKS  DIM3(40, 1, 1)
#define DEFAULT_THREADS DIM3(256, 1, 1)

#define CHECK_KERNEL_EXISTS(key, ret) \
  auto k    = key; \
  auto find = kernels.find(k); \
  if (find != kernels.end()) return ret \
// externalizes k and find
#define CHECK_KERNEL_DOES_NOT_EXIST(key, ret) \
  auto k    = key; \
  auto find = kernels.find(k); \
  if (find == kernels.end()) return ret \
// externalizes k and find

// TODO: this crap became thread_local because of the bank implementation...

// key, <callback, options>
static map<string, tuple<knlman_callback, int>> kernels;
// key, entryObj
static thread_local map<string, void*> entryObjs;
// key, list<bufferKey, options>
static thread_local map<string, list<tuple<const char*, int>>> bufferOptions;
// key, <blocks, threads>
static thread_local map<string, tuple<knlman_dim3_s, knlman_dim3_s>> kernelInfo;
// streamKey, <vectorOfStreams, currentStream>
static thread_local vector<cudaStream_t> streams;
static thread_local int currentStream;

static thread_local string selectedKernel;

// options for streaming
int knlman_create(const char *key, knlman_callback callback, int options)
{
  CHECK_KERNEL_EXISTS(string(key), -1);

  auto t = make_tuple(callback, options);
  kernels.insert(make_pair(k, t));

  selectedKernel = k;
  return 0;
}

int knlman_select(const char *key)
{
  CHECK_KERNEL_DOES_NOT_EXIST(string(key), -1);
  selectedKernel = k; // TODO: also externalize the iterator
  return 0;
}

int knlman_destroy()
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);
  kernels.erase(k);
  return 0;
}

int knlman_init_thread() { return 0; };

int knlman_init_control_thread() {
  /* TODO:setup the control thread */
  return 0;
}

int knlman_destroy_thread()
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);

  if (kernelInfo.find(k) != kernelInfo.end()) {
    kernelInfo.erase(k);
  }

  auto fndEO = entryObjs.find(k);
  if (fndEO != entryObjs.end()) {
    entryObjs.erase(k);
  }

  if (bufferOptions.find(k) != bufferOptions.end()) {
    bufferOptions.erase(k);
  }

  return 0;
}

int knlman_set_nb_blocks(int x, int y, int z)
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);
  auto fndI = kernelInfo.find(k);

  if (fndI == kernelInfo.end()) {
    // new value
    auto newT = make_tuple(DIM3(x, y, z), DEFAULT_THREADS);
    kernelInfo.insert(make_pair(k, newT));
  } else {
    // replace
    auto oldT = fndI->second;
    auto newT = make_tuple(DIM3(x, y, z), get<1>(oldT));
    fndI = kernelInfo.erase(fndI);
    kernelInfo.insert(fndI, make_pair(k, newT));
  }

  return 0;
}

int knlman_set_nb_threads(int x, int y, int z)
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);
  auto fndI = kernelInfo.find(k);

  if (fndI == kernelInfo.end()) {
    // new value
    auto newT = make_tuple(DEFAULT_BLOCKS, DIM3(x, y, z));
    kernelInfo.insert(make_pair(k, newT));
  } else {
    // replace
    auto oldT = fndI->second;
    auto newT = make_tuple(get<0>(oldT), DIM3(x, y, z));
    fndI = kernelInfo.erase(fndI);
    kernelInfo.insert(fndI, make_pair(k, newT));
  }

  return 0;
}

// this module does the copy automatically
int knlman_set_entry_object(void *hostArgs)
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);
  auto fndEO = entryObjs.find(k);

  if (fndEO == entryObjs.end()) {
    // not found
    bufferOptions[k].clear(); // empty list for buffers
    entryObjs.insert(make_pair(k, hostArgs));
  } else {
    // replace
    // old dev memory
    fndEO = entryObjs.erase(fndEO);
    entryObjs.insert(fndEO, make_pair(k, hostArgs));
  }

  return 0;
}

void* knlman_get_entry_object()
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, NULL);
  auto fndEO = entryObjs.find(k);

  if (fndEO != entryObjs.end()) {
    return fndEO->second;
  }

  return NULL;
}

int knlman_add_mem(const char *keyMem, int options)
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);

  auto newT = make_tuple(keyMem, options);
  bufferOptions[k].push_back(newT); // creates an empty list if not there

  return 0;
}

int knlman_add_stream()
{
  int res = streams.size();
  cudaStream_t stream; // this is a pointer

  CUDA_CHECK_ERROR(cudaStreamCreate(&stream), "stream");
  streams.push_back(stream);
  currentStream = res;

  return res;
}

int knlman_sync_stream()
{
  cudaStream_t stream = streams[currentStream];
  CUDA_CHECK_ERROR(cudaStreamSynchronize(stream), "sync");
  return 0;
}

int knlman_choose_stream(int streamKey)
{
  if (streamKey >= streams.size() || streamKey < 0) {
    int id = streamKey < 0 ? -streamKey : streamKey;
    currentStream = id % streams.size();
    return -1;
  }

  currentStream = streamKey;

  return 0;
}

int knlman_choose_next_stream()
{
  if (streams.empty()) {
    return -1;
  }

  currentStream = (currentStream+1)%streams.size();

  return 0;
}

void* knlman_get_current_stream()
{
  if (streams.empty()) {
    return NULL;
  }

  return streams[currentStream];
}

int knlman_is_stream_running(void *stream)
{
  cudaStream_t strm = (cudaStream_t)stream;
  return cudaStreamQuery(strm) == cudaErrorNotReady;
}

int knlman_destroy_streams()
{
  for (auto it : streams) {
    cudaStream_t stream = it;
    CUDA_CHECK_ERROR(cudaStreamDestroy(stream), "stream");
  }
  streams.clear();
  return 0;
}

int knlman_retrieve_mem()
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);
  auto findBOpt = bufferOptions.find(k);
  auto BOptList = findBOpt->second;
  int hasStream = streams.size() > 0;
  cudaStream_t stream = NULL;

  if (hasStream) {
    stream = streams[currentStream];
  }

  for (auto it = BOptList.begin(); it != BOptList.end(); ++it) {
    auto BOptMem = get<0>(*it);
    auto BOpt    = get<1>(*it);
    if (BOpt & (KNLMAN_COPY|KNLMAN_DtoH)) {
      memman_select(BOptMem);
      memman_cpy_to_cpu(stream);
    }
  }

  return 0;
}

int knlman_run()
{
  CHECK_KERNEL_DOES_NOT_EXIST(selectedKernel, -1);

  int hasStream = streams.size() > 0;
  cudaStream_t stream = NULL;
  list<string> afterList;

  if (hasStream) {
    stream = streams[currentStream];
  }

  auto findInfo = kernelInfo.find(k);
  auto findEObj = entryObjs.find(k);
  auto findBOpt = bufferOptions.find(k);
  auto t        = find->second;
  auto tInfo    = findInfo->second;
  auto EObj     = findEObj->second;
  auto BOptList = findBOpt->second;
  auto clbk     = get<0>(t);
  // auto clbkOpt  = get<1>(t); // not used
  auto nbBlocks = get<0>(tInfo);
  auto nbThread = get<1>(tInfo);

  for (auto it = BOptList.begin(); it != BOptList.end(); ++it) {
    auto BOptMem = get<0>(*it);
    auto BOpt    = get<1>(*it);
    if (BOpt & (KNLMAN_COPY|KNLMAN_HtoD)) {
      memman_select(BOptMem);
      memman_cpy_to_gpu(stream);
    }
  }

  // callback knows how to call the kernel
  clbk({
    .blocks   = nbBlocks,
    .threads  = nbThread,
    .stream   = stream,
    .entryObj = EObj
  });

  // TODO: DtoH and GPUsync must be done by the clbk

  return 0;
}
