CURR_DIR      ?= ~/projs/cuda-utils
CUDA_PATH     ?= /usr/local/cuda

CFG           ?= prod

INCLUDES := -I ./include \
	-I $(CUDA_PATH)/include
#
DEFINES  :=
#
LIBS     := -L $(CUDA_PATH)/lib64 -lcudart -lcuda
#
LIB  = libcuda-util.a
NVCC = $(CUDA_PATH)/bin/nvcc
CC   = gcc
CXX  = g++
AR   = ar rcs

### Add this if debug needed (GPU run much slower)
DEBUG_FLAGS  := -g -lineinfo
NV_DEB_FLAGS := -G -g -lineinfo

CFLAGS     := -c $(DEFINES) $(INCLUDES)
CXXFLAGS   := -c $(DEFINES) $(INCLUDES) -std=c++11
NVFLAGS    := -c $(DEFINES) $(INCLUDES) -std=c++11 \
	--default-stream per-thread \
	-arch sm_30
LDFLAGS    := $(LIBS)

ifeq ($(CFG),debug)
CFLAGS   += $(DEBUG_FLAGS)
CXXFLAGS += $(DEBUG_FLAGS)
NVFLAGS  += $(NV_DEB_FLAGS)
endif

ifeq ($(CFG),prod)
# not working with -O2
CFLAGS   += -O1
CXXFLAGS += -O1
NVFLAGS  += -O1
endif

SRC        := $(CURR_DIR)/src
BANK_OBJS  := \
	$(SRC)/bitmap.o \
	$(SRC)/memman.o \
	$(SRC)/knlman.o

.PHONY:	all clean

all: $(LIB)
	# done

$(LIB): $(BANK_OBJS)
	$(AR) $@ $(BANK_OBJS)

%.o:	%.c
	@echo ""
	$(CC) $(CFLAGS) -o $@ $<

%.o:	%.cpp
	@echo ""
	$(CXX) $(CXXFLAGS) -o $@ $<

%.o:	%.cu
	@echo ""
	$(NVCC) $(NVFLAGS) -o $@ $<

clean:
	rm -f $(LIB) *.o $(SRC)/*.o
