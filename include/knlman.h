#ifndef KNLMAN_H_GUARD_
#define KNLMAN_H_GUARD_

#ifdef __cplusplus
extern "C" {
#endif

#include "memman.h"

#define DIM3(x_, y_, z_) (knlman_dim3_s){.x = x_,  .y = y_, .z = z_}

typedef struct knlman_dim3_ {
  int x, y, z;
} knlman_dim3_s;

typedef struct knlman_callback_params_ {
  knlman_dim3_s blocks;
  knlman_dim3_s threads;
  void *stream;
  void *entryObj;
} knlman_callback_params_s;

// callback type
typedef void(*knlman_callback)(knlman_callback_params_s);

enum {
  KNLMAN_NONE  = 0b0000,
  KNLMAN_COPY  = 0b0001,
  KNLMAN_DtoH  = 0b0010,
  KNLMAN_HtoD  = 0b0100
};

// How this works:
//  -> User register a callback that "knows" how to launch the kernel
//     -> a pointer with relevant data can also be appended (EntryObject)

// TODO: add knlman_select(key) and remove the key as the first argument

// TODO: right know this library is mostly thread_local, only knlman_create
// is global.

// Set options to MEMMAN_NONE (future upgrade).
int knlman_create(const char *key, knlman_callback, int options); // also selects
int knlman_select(const char *key);
int knlman_destroy(); // only the callback

int knlman_init_thread();
int knlman_init_control_thread();
int knlman_destroy_thread(); // teardown thread local information


// WARNING: don't set x, y or z to 0, it is multiplicative
int knlman_set_nb_blocks(int x, int y, int z);
int knlman_set_nb_threads(int x, int y, int z);

// this module does the copy automatically: dev <--> host
// do not forget to set the pointers to buffers in the entry_object
// example:
//   obj = memman_get_cpu("entry_object")
//   obj.buffer1 = memman_get_gpu("b1")
//   obj.buffer2 = memman_get_gpu("other_buffer")
//   ...
// WARNING: memman buffers are not freed
int knlman_set_entry_object(void *hostArgs);
void* knlman_get_entry_object();
int knlman_add_mem(const char *keyMem, int options);

// Must be called before exec. Executes the kernel in the stream.
// Optionally add a callback to be executed after finish.
// WARNING: Streams must be created/destroyed per thread.
int knlman_add_stream(); // returns the streamKey, starts with 0
int knlman_sync_stream(); // waits the selected stream
int knlman_choose_stream(int keyStream);
int knlman_choose_next_stream();
void* knlman_get_current_stream();
int knlman_is_stream_running(void*);
int knlman_destroy_streams(); // destroy all streams

int knlman_retrieve_mem(); // copies DtoH if marked with COPY or DtoH

// TODO: stream handler: one thread is reponsible of starting kernels
// there is a call that add a kernel run callback (input parameters, job_run...)
//

// Executes the kernel. This functions does not wait the completion of the cuda kernel.
int knlman_run(); // copies HtoD and execute (does not copy HtoD)
// TODO: knlman_exec_prstm

#ifdef __cplusplus
}
#endif

#endif /* KNLMAN_H_GUARD_ */
