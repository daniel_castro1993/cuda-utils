#ifndef BITMAP_H_GUARD_
#define BITMAP_H_GUARD_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

// TODO: delete ........
#define LOG2_16BITS      4
#define LOG2_16BITS_MASK 0b1111
// .....................
#define LOG2_32BITS      5
#define LOG2_32BITS_MASK 0b11111

int bitmap_print(unsigned short *bitmap, size_t size, FILE *fp);

// here I used int because atomicOr is 32 or 64 bit
#define BM_SET_POS(pos, bm) ({ \
	unsigned int *BM_bitmap = (unsigned int*)bm; \
	unsigned long bmIdx = pos >> LOG2_32BITS; \
	unsigned long withinShort = LOG2_32BITS_MASK & pos; \
	unsigned int withinShortIdx = 1 << withinShort; \
	/*BM_bitmap[bmIdx] |= withinShortIdx;*/ \
	atomicOr(&BM_bitmap[bmIdx], withinShortIdx); \
}) \
//

#define BM_GET_POS(pos, bm) ({ \
	unsigned short *BM_bitmap = (unsigned short*)bm; \
	unsigned long bmIdx = pos >> LOG2_16BITS; \
	unsigned long withinShort = LOG2_16BITS_MASK & pos; \
	unsigned long withinShortIdx = 1 << withinShort; \
	BM_bitmap[bmIdx] & withinShortIdx; \
}) \
//

#define ByteM_SET_POS(pos, bm) ({ \
	unsigned char *BM_bitmap = (unsigned char*)bm; \
	BM_bitmap[pos] = (unsigned char)1; \
}) \
//

#define ByteM_GET_POS(pos, bm) ({ \
	unsigned char *BM_bitmap = (unsigned char*)bm; \
	BM_bitmap[pos]; \
}) \
//

// TODO: create a API for the bitmap
// uses the given buffer as bitmap
// int bitmap_init(void *buffer, size_t bufSize, size_t gran);

// does nothing
// int bitmap_destroy(void*);


#ifdef __cplusplus
}
#endif

#endif /* BITMAP_H_GUARD_ */
