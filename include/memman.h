#ifndef MEMMAN_H_GUARD_
#define MEMMAN_H_GUARD_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

enum {
  MEMMAN_NONE  = 0b0000,
  MEMMAN_ASYNC = 0b0001,
  MEMMAN_THRLC = 0b0010, // thread local memory
  MEMMAN_COPY  = 0b0100
};

// TODO: need a thread local ad-hoc allocator
//  -> map_to_gpu --> pushes in a stack the new buffer
//  -> map_delete --> pops ands frees the buffer

// alloc also selects
int memman_alloc_dual(const char *key, size_t size, int options);
int memman_alloc_gpu(const char *key, size_t size, void *host, int options);
int memman_alloc_cpu(const char *key, size_t size, void *dev, int options);

int memman_select(const char *key);

int memman_free_dual();
int memman_free_gpu();
int memman_free_cpu();

// attention using these in combination with frees
// use size == 0 to use the previous size
int memman_set_cpu(void *host, size_t size);
int memman_set_gpu(void *dev, size_t size);

void* memman_get_gpu(size_t *size);
void* memman_get_cpu(size_t *size);
int memman_zero_dual(void*); // pass NULL to use the sync version
int memman_zero_cpu(void*);
int memman_zero_gpu(void*);

// ad-hoc interface, if is the default stream use NULL
void* memman_ad_hoc_alloc(void *stream, void *host, size_t size); // pushes in stack (does not copy HtoD)
void* memman_ad_hoc_free(void *stream); // pops from stack (does not copy DtoH)
void* memman_ad_hoc_zero(void *stream); // zeros GPU memory only
void* memman_ad_hoc_cpy(void *stream); // copy HtoD
void* memman_ad_hoc_retrieve(void *stream); // copy DtoH

// receives a cudaStream_t
int memman_cpy_to_cpu(void*);
int memman_cpy_to_gpu(void*);

#ifdef __cplusplus
}
#endif

#endif /* MEMMAN_H_GUARD_ */
