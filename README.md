# cuda-utils

This repository offers some utility code to be used in CUDA projects.

---

## Compile on remote servers

You can use the provided `compile_in_node.sh` script. Provided that `ssh` to the CUDA enabled server is properly configured, you just need to change your username and target server name.

